const cvs = document.getElementById("canvas");
const ctx = cvs.getContext("2d");
const [cw, ch] = [cvs.width, cvs.height];
const tw = 0.5;
const td = 1.5;

const iPacman = document.getElementById("iPacman");
const iGhosts = document.getElementById("iGhosts");
const iFont = document.getElementById("iFont");

const aMunch1 = document.getElementById("aMunch1");
const aMunch2 = document.getElementById("aMunch2");
const sirens = [
    document.getElementById("aSiren1"),
    document.getElementById("aSiren2"),
    document.getElementById("aSiren3"),
    document.getElementById("aSiren4"),
    document.getElementById("aSiren5"),
];
const aGameStart = document.getElementById("aGameStart");
const aDeath1 = document.getElementById("aDeath1");
const aDeath2 = document.getElementById("aDeath2");
const blueSirens = sirens;
const aPowerPellet = document.getElementById("aPowerPellet");
const retreating = [
    document.getElementById("aRetreating1"),
    document.getElementById("aRetreating2"),
    document.getElementById("aRetreating3"),
    document.getElementById("aRetreating4"),
    document.getElementById("aRetreating5"),
];
const aEatGhost = document.getElementById("aEatGhost");
const aIntermission = document.getElementById("aIntermission");

const allSounds = [
    aMunch1, aMunch2, aGameStart, aDeath1, aDeath2, aPowerPellet, aEatGhost,
].concat(sirens).concat(retreating);

function muteAll() {
    allSounds.forEach(s => s.muted = true);
}

function unmuteAll() {
    allSounds.forEach(s => s.muted = false);
}

let muted = false;

function toggleMute() {
    muted = !muted;
    if(muted) muteAll();
    else unmuteAll();
}

let lives, score, neaten;
let pacFrame = 0;
let keys = [];

let rmap, rx, ry;

let pac, blinky, pinky, inky, clyde, spunky;
let ghosts;

let nowPlaying = null;

let pause = false;
let pauseGhosts = false;

let radar = [];
let radAlpha = 0;

let scoreAnimations = [];

const blueModeLength = 250;

let updateInterval;

function addScore(s) {
    scoreAnimations.push([""+s, 1]);
    score += s;
}

const colours = [
    [0,0,255],
    [0,255,0],
    [127,0,0],
    [255,255,0],
    [255,0,255],
    [0,255,255],
];

function mod(n, d) {
    while(n < 0) n += d;
    return n % d;
}

function quad(x1, y1, x2, y2, x3, y3, x4, y4) {
    ctx.beginPath();
    ctx.moveTo(x1*cw, y1*ch);
    ctx.lineTo(x2*cw, y2*ch);
    ctx.lineTo(x3*cw, y3*ch);
    ctx.lineTo(x4*cw, y4*ch);
    ctx.fill();
}

function rect(x1, y1, w, h) {
    ctx.fillRect(x1*cw, y1*ch, w*cw, h*ch);
}

function srect(x1, y1, w, h) {
    ctx.strokeRect(x1*cw, y1*ch, w*cw, h*ch);
}

function line(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1*cw, y1*ch);
    ctx.lineTo(x2*cw, y2*ch);
    ctx.stroke();
}

function pline(x1, y1, x2, y2) {
    let of = ctx.fillStyle;
    ctx.fillStyle = ctx.strokeStyle;
    ctx.beginPath();
    ctx.moveTo(x1*cw, y1*ch-ctx.lineWidth/2);
    ctx.lineTo(x2*cw, y2*ch-ctx.lineWidth/2);
    ctx.lineTo(x2*cw, y2*ch+ctx.lineWidth/2);
    ctx.lineTo(x1*cw, y1*ch+ctx.lineWidth/2);
    ctx.fill();
    ctx.fillStyle = of;
}

function vpline(x1, y1, x2, y2) {
    let of = ctx.fillStyle;
    ctx.fillStyle = ctx.strokeStyle;
    ctx.beginPath();
    ctx.moveTo(x1*cw-ctx.lineWidth/2, y1*ch);
    ctx.lineTo(x2*cw-ctx.lineWidth/2, y2*ch);
    ctx.lineTo(x2*cw+ctx.lineWidth/2, y2*ch);
    ctx.lineTo(x1*cw+ctx.lineWidth/2, y1*ch);
    ctx.fill();
    ctx.fillStyle = of;
}

function drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh) {
    ctx.drawImage(img, sx, sy, sw-0.5, sh-0.5, dx*cw, dy*ch, dw*cw, dh*ch);
}

function clear() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, cw, ch);
}

function darken() {
    ctx.fillStyle = "rgb(0,0,0,0.5)";
    ctx.fillRect(0, 0, cw, ch);
}

function drawText(s, x, y, tw, th) {
    const ss = 64;
    for(let i = 0; i < s.length; i++) {
        let c = s.charCodeAt(i);
        drawImage(iFont, (c%32)*ss, div(c,32)*ss, ss, ss, x+i*tw, y, tw, th);
    }
}

function prj(x, z) {
    return (x-0.5)/z+0.5;
}

function shade(b, z) {
    return b/Math.max(z*z/2, 1);
}

function drawBlock(x, w, z, zh, t, left=false, front=false) {
    const y = 0.5;
    const h2 = 0.5;
    ctx.lineWidth = cvs.height*0.02/(Math.max(z/2, 1));
    let rgb = colours[t-1];
    if(t==3) ctx.lineWidth /= 2;
    ctx.strokeStyle =
        "rgb("+shade(rgb[0],z)+","+shade(rgb[1],z)+","+shade(rgb[2],z)+")";
    if(pac.r%2 == 0) ctx.fillStyle = "#000008";
    else ctx.fillStyle = "#00000e";
    if(z < 0) return;
    let z1 = z;
    let z2 = z+zh;
    let [y1, y2] = [y-h2, y+h2].map(y => prj(y, z1));
    let [y3, y4] = [y-h2, y+h2].map(y => prj(y, z2));
    let [y5, y6] = [y-h2+0.02, y+h2-0.02].map(y => prj(y, z1));
    let [y7, y8] = [y-h2+0.02, y+h2-0.02].map(y => prj(y, z2));
    let [y9, y10] = [y-h2+0.12, y+h2-0.12].map(y => prj(y, z1));
    let [y11, y12] = [y-h2+0.12, y+h2-0.12].map(y => prj(y, z2));
    let [x1, x2, x3] = [prj(x, z1), prj(x+w/2, z1), prj(x+w, z1)];
    let [x4, x5, x6] = [prj(x, z2), prj(x+w/2, z2), prj(x+w, z2)];
    let avx = (x2+x5)/2;
    if(avx < 0.5) {
        quad(x3, y1, x6, y3, x6, y4, x3, y2);
        pline(x3, y5, x6, y7);
        pline(x3, y6, x6, y8);
        pline(x3, y9, x6, y11);
        pline(x3, y10, x6, y12);
    } else if(!left) {
        quad(x1, y1, x4, y3, x4, y4, x1, y2);
        pline(x1, y5, x4, y7);
        pline(x1, y6, x4, y8);
        pline(x1, y9, x4, y11);
        pline(x1, y10, x4, y12);
    }
    if(avx < 0.25) {
        rect(0, y1, x1, y2-y1);
    } else if(avx > 0.75) {
        rect(x3, y1, 1-x3, y2-y1);
    }
    if(!front) {
        if(pac.r%2 != 0) ctx.fillStyle = "#000008";
        else ctx.fillStyle = "#00000e";
    }
    quad(x1, y1, x3, y1, x3, y2, x1, y2);
    if(!front) {
        if(y1 > 0.12) {
            line(x1, y6, x3, y6);
            line(x1, y5, x3, y5);
        }
        if(y1 > 0.02) {
            line(x1, y9, x3, y9);
            line(x1, y10, x3, y10);
        }
    }
}

function drawMarker(x, w, z, zh) {
    const y = 1;
    const h2 = 0.5;
    if(z < -0.1) return;
    if(z < 0) z = 0;
    let z1 = z;
    let z2 = z+zh*0.25;
    let z3 = z+zh*0.75;
    let z4 = z+zh;
    //ctx.fillStyle = "rgb("+64/Math.max(1, z/10)+",0,0)";
    ctx.fillStyle = "#400000";
    let [y1, y2, y3, y4] = [prj(y, z1), prj(y, z2), prj(y, z3), prj(y, z4)];
    let xb = [x, x+w*0.25, x+w*0.75, x+w];
    let [x1, x2, x3, x4] = [
        xb.map(x => prj(x, z1)), xb.map(x => prj(x, z2)),
        xb.map(x => prj(x, z3)), xb.map(x => prj(x, z4)),
    ];
    if(pac.r%2 != 0) {
        quad(x2[0], y2, x3[0], y3, x3[3], y3, x2[3], y2);
    } else {
        quad(x1[1], y1, x4[1], y4, x4[2], y4, x1[2], y1);
    }
}

function drawPellet(x, z) {
    const w = 0.2;
    const h = 0.35;
    const y = 0.8;
    if(z < 0) return;
    let s = shade(1024, z);
    let [px, py] = [prj(x, z), prj(y, z)];
    let [pw, ph] = [w/(z*2), h/(z*2)];
    ctx.fillStyle = "rgb(" + s + "," + s + "," + Math.min(s/2, 128) + ")";
    rect(px-pw/2, py-ph/2, pw, ph);
}

function drawBluePellet(x, z) {
    const w1 = 0.2*1.5;
    const h1 = 0.35*1.5;
    const w2 = 0.2*2;
    const h2 = 0.35*2;
    const y = 0.8;
    if(z < 0) return;
    let s = shade(1024, z);
    let [px, py] = [prj(x, z), prj(y, z)];
    let [pw1, ph1] = [w1/(z*2), h1/(z*2)];
    let [pw2, ph2] = [w2/(z*2), h2/(z*2)];
    ctx.fillStyle = "rgb(" + s + "," + s + "," + Math.min(s/1.5, 196) + ")";
    rect(px-pw1/2, py-ph2/2, pw1, ph2);
    rect(px-pw2/2, py-ph1/2, pw2, ph1);
}

function mapTile(x, y, m=map) {
    return m[mod(y, mapsz)*mapsz+mod(x, mapsz)];
}

function setMapTile(x, y, t, m=map) {
    m[mod(y, mapsz)*mapsz+mod(x, mapsz)] = t;
}

function rot90(x, y) {
    return [mapsz-1-y, x];
}

function rotMap(mmap) {
    let rm = [];
    for(let i = 0; i < mapsz*mapsz; i++) {
        let [x, y] = rot90(i%mapsz, Math.floor(i/mapsz));
        rm[y*mapsz+x] = mmap[i];
    }
    return rm;
}

class Actor {
    constructor(x, y, ghost=0) {
        this.x = x;
        this.y = y;
        this.dx = 0;
        this.dy = 0;
        this.r = 0;
        this.ghost = ghost;
        this.delay = 0;
        this.tx = -1;
        this.ty = -1;
        this.coasting = 0;
        this.intel = 0;
        this.dead = false;
        this.blue = 0;
    }

    moving() {
        return (this.dx != 0 || this.dy != 0);
    }

    move(xm, ym) {
        if(this.moving()) return;
        let dx = Math.floor(this.x+xm);
        let dy = Math.floor(this.y+ym);
        let t = mapTile(dx, dy);
        if(t > 0) return;
        if(this == pac && t == tDen) return;
        this.dx = xm*0.1;
        this.dy = ym*0.1;
    }

    forward() {
        this.move(dirs[this.r][0], dirs[this.r][1]);
    }

    rotate(r) {
        this.rr = mod(this.r-r, 4);
        this.rx = this.x+this.dx;
        this.ry = this.y+this.dy;
        for(let i = 0; i < 4-r; i++)
            [this.rx, this.ry] = rot90(this.rx, this.ry);
    }

    draw(x, z) {
        if(z < 0) return;
        const y = 0.5;
        let w = 1.0/(z*2);
        let h = 1.6/(z*2);
        x = prj(x, z);
        let g = (this.blue) ?
            ((this.blue <= blueModeLength*0.2) ? 6 : 5)
            : ((this.dead) ? 7 : this.ghost);
        if(!this.dead)
            drawImage(iGhosts, 0, 512, 64, 64, x-w/2, y-h/2, w, h);
        ctx.globalAlpha = 1/Math.max(z*z/2,1);
        drawImage(iGhosts, this.rr*64, g*64, 64, 64,
            x-w/2, y-h/2, w, h);
        ctx.globalAlpha = 1;
    }

    navGen(tx, ty) {
        //if(this.moving()) return [[], -1];
        let pmap = [];
        for(let i = 0; i < mapsz*mapsz; i++) {
            pmap[i] = (map[i]>=1) ? -1 : 0;
        }
        pmap[ty*mapsz+tx] = 1;
        let working;
        let g = 1;
        do {
            working = false;
            for(let i = 0; i < mapsz*mapsz; i++) {
                if(pmap[i] != g) continue;
                for(let j = 0; j < 4; j++) {
                    let x = mod(i%mapsz+dirs[j][0],mapsz);
                    let y = mod(div(i,mapsz)+dirs[j][1],mapsz);
                    if(pmap[y*mapsz+x] == 0) {
                        pmap[y*mapsz+x] = g+1;
                        working = true;
                    }
                    if(x == this.x && y == this.y) {
                        return [pmap, j];
                    }
                }
            }
            g++;
        } while(working);
        /*let s = "";
        for(let y = 0; y < mapsz; y++) {
            for(let x = 0; x < mapsz; x++)
                s += (pmap[y*mapsz+x]==-1) ? "##" : pmap[y*mapsz+x]+" ";
            s += "\n";
        }
        console.log(s);*/
        return [pmap, -1];
    }

    navTo(tx, ty) {
        let [pmap, d] = this.navGen(tx, ty);
        if(d == -1) return;
        let lr = this.r;
        this.r = (d+2)%4;
        if(this.r != lr) this.delay = 2;
        this.move(-dirs[d][0], -dirs[d][1]);
    }

    distance(tx, ty) {
        let [pmap, d] = this.navGen(tx, ty);
        if(d == -1) return mapsz*mapsz;
        return pmap[this.y*mapsz+this.x]-1;
    }

    crow(x, y) {
        let xd = x-(this.x+this.dx);
        let yd = y-(this.y+this.dy);
        return xd*xd+yd*yd;
    }

    target(x, y) {
        this.tx = x;
        this.ty = y;
        this.coasting = 0;
    }

    blinky() {
        if(this.coasting||this.dead) return;
        if(this.blue) { this.target(corner[0][0], corner[0][1]); return; }
        this.target(pac.x, pac.y);
        this.coasting = rand(5);
    }

    clyde() {
        if(this.coasting||this.dead) return;
        if(this.blue) { this.target(corner[1][0], corner[1][1]); return; }
        if(this.distance(pac.x, pac.y) < 8) this.target(1, mapsz-2);
        else this.target(pac.x, pac.y);
        this.coasting = 10+rand(30);
    }

    inky() {
        if(this.coasting||this.dead) return;
        if(this.blue) { this.target(corner[2][0], corner[2][1]); return; }
        if(this.distance(blinky.x, blinky.y) > this.distance(pinky.x, pinky.y))
            this.target(blinky.x, blinky.y);
        else this.target(pinky.x, pinky.y);
    }

    pinky() {
        if(this.coasting||this.dead) return;
        if(this.blue) { this.target(corner[3][0], corner[3][1]); return; }
        if(this.intel && this.tx != -1) { this.intel--; return; }
        if(this.distance(pac.x, pac.y) < 5) { this.target(pac.x, pac.y); return; }
        let r = pac.r;
        let x = pac.x;
        let y = pac.y;
        for(let d = 0; d < 10 && r != (pac.r+2)%4; d++) {
            let nx = x+dirs[r][0];
            let ny = y+dirs[r][1];
            let br = (r+2)%4;
            let c = 0;
            while(mapTile(nx, ny) >= 1 || r == br) {
                r = (r+1)%4;
                nx = x+dirs[r][0];
                ny = y+dirs[r][1];
                if(c++ >= 4) { this.coasting = 50; return; }
            }
            y = ny;
            x = nx;
            /*if(d == 0) continue;
            let cx, cy;
            cx = x+dirs[(pac.r+1)%2][0];
            cy = y+dirs[(pac.r+1)%2][1];
            if(mapTile(cx, cy) < 1) { this.target(x, y); break; }
            cx = x+dirs[(pac.r-1)%2][0];
            cy = y+dirs[(pac.r-1)%2][1];
            if(mapTile(cx, cy) < 1) { this.target(x, y); break; }*/
        }
        this.target(x, y);
        this.intel = 5;
    }

    spunky() {
        if(this.coasting||this.dead) return;
        if(this.blue) { this.target(corner[4][0], corner[4][1]); return; }
        this.blinky();
    }

    eat() {
        this.blue = 0;
        this.dead = true;
        this.target(denx, deny);
        this.coasting = 0;
        addScore(200*Math.pow(2, neaten));
        if(neaten < 4) neaten++;
        if(!pause) aEatGhost.play();
    }

    update(speed=0.01) {
        if(this.blue > blueModeLength*0.2) { if(!pause) aPowerPellet.play(); speed /= 2; }

        if(this.coasting) this.coasting--;
        if(this.delay != 0) { this.delay--; return; }

        if(this.dead) {
            if(this.tx == -1) this.dead = false;
            else speed *= 1.5;
        }

        if(this.blue) this.blue--;

        if(this == spunky) speed /= 2;

        let wasMoving = this.dx || this.dy;

        if(this.dx > 0) this.dx += speed;
        if(this.dx < 0) this.dx -= speed;
        if(this.dy > 0) this.dy += speed;
        if(this.dy < 0) this.dy -= speed;
        if(this.dx >= 1) { this.dx = 0; this.x = mod(this.x+1, mapsz); }
        if(this.dx <= -1) { this.dx = 0; this.x = mod(this.x-1, mapsz); }
        if(this.dy >= 1) { this.dy = 0; this.y = mod(this.y+1, mapsz); }
        if(this.dy <= -1) { this.dy = 0; this.y = mod(this.y-1, mapsz); }

        if(this.x == this.tx && this.y == this.ty) {
            this.tx = -1;
            if(this != pac && this.blue) this.coasting = 50;
        }

        if(this == pac) {
            function munch() {
                if((pac.y%2+pac.x)%2) {
                    aMunch2.pause();
                    aMunch1.currentTime = 0;
                    if(!pause) aMunch1.play();
                } else {
                    aMunch1.pause();
                    aMunch2.currentTime = 0;
                    if(!pause) aMunch2.play();
                }
            }

            if(mapTile(this.x, this.y) == tPellet) {
                setMapTile(this.x, this.y, 0);
                addScore(10);
                if(++npellets >= totalPellets) {
                    nowPlaying = aIntermission;
                    scoreAnimations = [];
                    draw();
                    aIntermission.onended = function() {
                        nowPlaying = null;
                        generateMap();
                        spawnGame();
                    }
                    if(!pause) aIntermission.play();
                    return;
                }
                munch();
            } else if(mapTile(this.x, this.y) == tBluePellet) {
                setMapTile(this.x, this.y, 0);
                ghosts.forEach(g => g.blue = (g.dead) ? 0 : blueModeLength);
                addScore(50);
                munch();
            }

            for(let i = 0; i < ghosts.length; i++) {
                let g = ghosts[i];
                if(pac.crow(g.x+g.dx, g.y+g.dy) < 0.8) {
                    if(g.blue) g.eat();
                    else if(!g.dead) { killPacman(); break; }
                }
            }
        } else {
            if(this.tx != -1) this.navTo(this.tx, this.ty);
            else {
                for(;;) {
                    this.move(dirs[this.r][0], dirs[this.r][1]);
                    if(!this.moving()) { this.r = (this.r+1)%4; this.delay = 2; }
                    else break;
                }
            }

            sirens[this.ghost].pause();
            blueSirens[this.ghost].pause();
            retreating[this.ghost].pause();
            let s = (this.blue) ?
                blueSirens[this.ghost] :
                ((this.dead) ? retreating[this.ghost] : sirens[this.ghost])
            if(!this.delay && !pause) s.play();
            let d = Math.max(this.crow(pac.x+pac.dx, pac.y+pac.dy)/4, 1);
            s.volume = 1/d;
        }
    }
}

function pauseSfx() {
    allSounds.forEach(s => s.pause());
}

function spawnGame() {
    nowPlaying = aGameStart;
    getReady = true;
    pac = new Actor(corner[4][0], corner[4][1]);
    pac.r = 1;
    blinky = new Actor(denx-2, deny, 0);
    pinky = new Actor(denx-1, deny, 1);
    inky = new Actor(denx, deny, 2);
    clyde = new Actor(denx+1, deny, 3);
    spunky = new Actor(denx+2, deny, 4);
    ghosts = [blinky, pinky, inky, clyde, spunky];
    ghosts.forEach(g => g.delay = 25);
    pauseSfx();
    this.blue = 0;
    if(map[pac.y*mapsz+pac.x] == tPellet) {
        map[pac.y*mapsz+pac.x] = 0;
        npellets++;
        score += 10;
    }
    radAlpha = 0;
    draw();
    scoreAnimations = [];
    aGameStart.onended = function() { nowPlaying = null; }
    if(!pause) aGameStart.play();
}

function killPacman() {
    nowPlaying = aDeath1;
    pauseSfx();
    if(--lives <= 0) { /* game over */
        clearInterval(updateInterval);
        updateInterval = null;
        aDeath1.onended = function() {
            nowPlaying = null;
            clear();
            drawMap();
            darken();
            drawText("GAME OVER", 0.5-0.05*9, 0.2, 0.1, 0.2);
            drawText("PRESS R TO RESTART", 0.5-0.0125*18, 0.5-0.025, 0.025, 0.05);
            let s = "SCORE "+score;
            drawText(s, 0.5-0.0375*s.length, 0.8, 0.075, 0.15);
            aDeath2.onended = function() { aDeath2.onended = null; aDeath2.play(); }
            aDeath2.play();
        }
    } else {
        scoreAnimations = [];
        draw();
        aDeath1.onended = function() { nowPlaying = null; spawnGame(); }
    }
    if(!pause) aDeath1.play();
}

function drawTileL(x, y) {
    let t = mapTile(x, y, rmap);
    if(t <= 0 && t != tDen && ((y == div(mapsz,2) && pac.r%2!=0)
            || (x == div(mapsz,2) && pac.r%2==0)))
        drawMarker((x-rx-0.5)*tw+0.5, tw, (ry-y)*td+0.25, td);
}

function drawTile(x, y) {
    if(mapTile(x, y, rmap) > 0)
        drawBlock((x-rx-0.5)*tw+0.5, tw, (ry-y)*td+0.25, td, mapTile(x,y,rmap),
            mapTile(x-1, y, rmap) > 0, mapTile(x, y+1, rmap) > 0);
    else if(mapTile(x, y, rmap) == tPellet)
        drawPellet((x-rx-0.5)*tw+0.5+tw/2, (ry-y)*td+0.25+td/2);
    else if(mapTile(x, y, rmap) == tBluePellet)
        drawBluePellet((x-rx-0.5)*tw+0.5+tw/2, (ry-y)*td+0.25+td/2);
}


function drawGhosts(y) {
    let todraw = [];
    ghosts.forEach(function(g) {
        if(g.ry > y-0.5 && g.ry <= y+0.5) todraw.push(g);
    });

    let f;
    do {
        f = 0;
        for(let i = 1; i < todraw.length; i++)
            if(todraw[i-1].ry > todraw[i].ry) {
                f = todraw[i-1];
                todraw[i-1] = todraw[i];
                todraw[i] = f;
            }
    } while(f != 0);

    todraw.forEach(g =>
        g.draw((g.rx-rx-0.5)*tw+0.5+tw/2,
            mod(ry-g.ry,mapsz)*td-0.25+td/2));
}

function drawMap() {
    pac.rotate(pac.r);
    ghosts.forEach(g => g.rotate(pac.r));

    rx = pac.rx;
    ry = pac.ry;
    rmap = map;
    for(let i = 0; i < 4-pac.r; i++) {
        rmap = rotMap(rmap);
    }

    const h = 9;
    const w2 = 3;
    for(let y = Math.floor(ry-h); y <= ry+0; y++) {
        for(let x = Math.floor(rx-w2); x <= Math.floor(rx+w2); x++)
            drawTileL(x, y);
        for(let x = Math.floor(rx-w2); x <= Math.floor(rx); x++)
            drawTile(x, y);
        for(let x = Math.floor(rx+w2); x > Math.floor(rx); x--)
            drawTile(x, y);
        drawGhosts(mod(y,mapsz));
    }

    /*ctx.fillStyle = "#000080";
    for(let y = 0; y < mapsz; y++)
        for(let x = 0; x < mapsz; x++)
            if(rmap[y*mapsz+x] > 0)
                rect(x*0.01, y*0.015, 0.01, 0.015)
    ctx.fillStyle = "white";
    rect(rx*0.01+0.0025, ry*0.015+0.00375, 0.005, 0.0075);*/
}

function drawMinimap() {
    const w = 0.2;
    const h = 0.3;
    const x = 1-w-0.01;
    const y = 0.02;
    const gw = 0.0125;
    const gh = 0.02;
    const z = 2;
    ctx.fillStyle = "#000000";
    ctx.strokeStyle = "#ffffff";
    ctx.lineWidth = ch*0.004;
    rect(x, y, w, h);
    ctx.globalAlpha = 0.25;
    line(x+w/2, y, x+w/2, y+h);
    line(x, y+h/2, x+w, y+h/2);
    ctx.globalAlpha = 1;
    srect(x, y, w, h);
    ctx.fillStyle = "#ffffff";
    radar.forEach(function(r) {
        ctx.fillStyle = "rgba(255, 255, 255, "+radAlpha*radAlpha*r[0]+")";
        rect(r[1]-gw*r[0]/2, r[2]-gh*r[0]/2, gw*r[0], gh*r[0]);
    });
}

function drawLife(x, y, w, h) {
    let f = [2, 1, 0, 1][Math.abs(pacFrame-2)%4];
    drawImage(iPacman, f*32, pac.r*32, 32, 32, x, y, w, h);
}

function drawLives() {
    const lw = 0.04;
    const lh = 0.065;
    for(let i = 0; i < lives; i++)
        drawLife(i*lw, 1-lh, lw, lh);
}

function draw() {
    clear();
    drawMap();
    drawLives();
    drawMinimap();

    let scoreText = "SCORE "+score;
    drawText(scoreText, 0, 0, 0.04, 0.08);
    ctx.fillStyle = "#ffff88";
    rect(0.02*0.2, 0.08+0.04*0.4, 0.02*0.6, 0.04*0.6);
    drawText(npellets+"/"+totalPellets, 0.02, 0.08+0.01, 0.02, 0.04);

    scoreAnimations.forEach(function(s) {
        ctx.globalAlpha = s[1];
        drawText(s[0], 0.04*6, 0.08+s[1]*0.16, 0.02, 0.04);
    });
    ctx.globalAlpha = 1;
}

function togglePause() {
    if(updateInterval == null) return;
    pause = !pause;
    if(pause) {
        pauseSfx();
        darken();
        drawText("PAUSE", 0.5-0.15*2.5, 0.5-0.1, 0.15, 0.2);
    } else if(nowPlaying != null) {
        nowPlaying.play();
        draw();
    }
}

addEventListener("keydown", function(e) {
    let k = e.keyCode;
    keys[k] = true;

    switch(k) {
    case 80: togglePause(); break;
    case 77: toggleMute(); break;
    case 82: restartGame(); break;
    }

    if(pause||nowPlaying!=null||lives<=0) return;

    switch(k) {
    case 37: case 65: pac.r = mod(pac.r-1, 4); draw; break;
    case 39: case 68: pac.r = mod(pac.r+1, 4); draw(); break;
    case 40: case 83: pac.r = mod(pac.r+2, 4); draw(); break;
    }
});

addEventListener("keyup", function(e) {
    keys[e.keyCode] = false;
});

let ghostUpdateDelay = 0;

function updateMovement() {
    const psp = 0.240;
    const gsp = 0.280;
    if(keys[38]||keys[87]) pac.forward();
    if(!pauseGhosts) {
        if(!ghostUpdateDelay) {
            blinky.blinky();
            clyde.clyde();
            inky.inky();
            pinky.pinky();
            spunky.spunky();
            ghostUpdateDelay = 0;//15+rand(30);
        } else ghostUpdateDelay--;
        ghosts.forEach(g => g.update(gsp));
    }
    pac.update(psp);
}

function updatePacFrame() {
    if(pac.moving() || Math.abs(pacFrame-2) != 1)
        pacFrame = (pacFrame+1)%5;
}

function radXY(rrx, rry) {
    const w = 0.2;
    const h = 0.3;
    const x = 1-w-0.01;
    const y = 0.02;
    const z = 2;
    let gx = x-w*z/4+(mod(rrx-rx+div(mapsz,2),mapsz,2)+0.5)/mapsz*w*z;
    let gy = y-h*z/4+(mod(rry-ry+div(mapsz,2),mapsz,2)+0.5)/mapsz*h*z;
    return [gx, gy];
}

function updateRadar() {
    if(radAlpha > 0) { radAlpha -= 0.1; return; }
    const w = 0.2;
    const h = 0.3;
    const x = 1-w-0.01;
    const y = 0.02;
    radAlpha = 1;
    radar = [];
    for(let i = 0; i < mapsz*mapsz; i++) {
        let sz;
        if(rmap[i] == tPellet) sz = 0.2;
        else if(rmap[i] == tBluePellet) sz = 0.4;
        else continue;
        let [px, py] = radXY(i%mapsz, div(i,mapsz));
        if(px > x && py > y && px < x+w && py < y+h)
            radar.push([sz, px, py]);
    }
    ghosts.forEach(function(g) {
        let [gx, gy] = radXY(g.rx, g.ry);
        if(gx > x && gy > y && gx < x+w && gy < y+h)
            radar.push([1, gx, gy]);
    });
}

function updateScoreAnimations() {
    for(let i = 0; i < scoreAnimations.length; i++) {
        scoreAnimations[i][1] -= 0.1;
        if(scoreAnimations[i][1] <= 0) {
            if(scoreAnimations.length > 1 && i != scoreAnimations.length-1)
                scoreAnimations[i] = scoreAnimations.pop();
            else scoreAnimations.pop();
            i--;
        }
    }
}

function update() {
    if(pause || nowPlaying != null) return;
    aPowerPellet.pause();
    updateMovement();
    updateRadar();
    updatePacFrame();
    updateScoreAnimations();
    draw();
}

function startGame() {
    let start = document.getElementById("start");
    start.className = "hidden";
    cvs.style.visibility = "visible";

    lives = 3;
    score = 0;
    neaten = 0;

    generateMap();
    spawnGame();
    draw();
    updateInterval = setInterval(update, 80);

    //while(map[div(mapsz,2)-1] == -1) restartGame();
}

function restartGame() {
    clearInterval(updateInterval);
    updateInterval = null;
    if(nowPlaying != null) {
        nowPlaying.onended = null;
        nowPlaying.currentTime = 0;
        nowPlaying = null;
    }
    pause = false;
    pauseSfx();
    startGame();
}
