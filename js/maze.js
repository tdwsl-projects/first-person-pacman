
function div(a, b) {
    return Math.floor(a/b);
}

function rand(r) {
    return Math.floor(Math.random()*r);
}

const dirs = [[0, -1], [1, 0], [0, 1], [-1, 0]];

let bmap = [];
const bh = 17;
const bw = div(bh, 2)+1
const seedn = 32;
const seede = 1;

const tPellet = -1;
const tBluePellet = -2;
const tDen = -3;
const minPellets = 350;

let mapsz = bh*2-1;
let map = [];

let corner;

let totalPellets, npellets;

let denx, deny;

function bInBounds(x, y) {
    return (x >= 0 && y >= 0 && x < bw && y < bh);
}

function bget(x, y) {
    if(bInBounds(x, y)) return bmap[y*bw+x];
    else return -1;
}

function bset(x, y, t) {
    if(bInBounds(x, y)) bmap[y*bw+x] = t;
}

function brect(x1, y1, w, h, t) {
    let x2 = x1+w;
    let y2 = y1+h;
    for(let x = x1; x < x2; x++)
        for(let y = y1; y < y2; y++)
            bset(x, y, t);
}

function nadjacent(x, y, t) {
    let n = 0;
    for(let i = 0; i < 4; i++)
        if(bget(x+dirs[i][0], y+dirs[i][1]) == t) n++;
    return n;
}

function adjacent(x, y, t) {
    let arr = [];
    for(let i = 0; i < 4; i++)
        if(bget(x+dirs[i][0], y+dirs[i][1]) == t)
            arr.push([x+dirs[i][0], y+dirs[i][1]]);
    return arr;
}

function shuffle(arr) {
    let nu = [];
    while(arr.length != 0) {
        let i = rand(arr.length);
        nu.push(arr[i]);
        arr = arr.slice(0, i).concat(arr.slice(i+1, arr.length));
    }
    return nu;
}

function placeSeed(t) {
    let i;
    for(let tr = 0; tr < 100; tr++) {
        i = rand(bw*bh);
        tr++;
        if(bmap[i] == 0) { bmap[i] = t; return; }
    }
    while(bmap[i] != 0) { i = (i+1)%(bw*bh); }
    bmap[i] = t;
}

function expand(t, madj=2, max=10) {
    let i = rand(bw*bh)-1;
    let n = 0;
    for(let j = 0; j < bw*bh; j++) {
        i = (i+1)%(bw*bh);
        if(bmap[i] != t) continue;
        if(nadjacent(i%bw, div(i,bw), t) > madj) continue;
        adj = shuffle(adjacent(i%bw, div(i,bw), 0));
        if(adj.length != 0) bmap[adj[0][1]*bw+adj[0][0]] = -1;
        if(++n > max) break;
        /*adj.every(function(a) {
            if(nadjacent(a[0], a[1], t) > 2) return true;
            bmap[a[1]*bw+a[0]] = -1;
            return false;
        });*/
    }
    bmap = bmap.map(l => (l==-1) ? t : l);
}

function expandAll(madj=2, max=10) {
    for(let i = 2; i < 36; i++) expand(i, madj, max);
}

function generateBase() {
    for(let i = 0; i < bw*bh; i++) bmap[i] = 0;
    brect(bw-4, div(bh, 3)-1, 5, 5, 1);

    bmap[1*bw+1] = 2;
    bmap[2*bw+1] = 2;
    bmap[1*bw+2] = 2;
    bmap[(bh-1)*bw+1] = 2;
    bmap[(bh-2)*bw+1] = 2;
    bmap[(bh-1)*bw+2] = 2;
    //expand(2, 2, 4);

    //brect(bh-2, 1, bw, 1, 2);

    for(let i = 3; i < seedn; i++)
        for(let j = 0; j < seede; j++)
            placeSeed(i);

    for(let g = 0; g < 100; g++) {
        expandAll(1);
    }
    //expandAll(3, 100);

    bmap = bmap.map(t => (t==0) ? 2 : t);

    brect(bw-3, div(bh, 3), 3, 3, 0);
    /*brect(bw-4, bh-1, 5, 1, 2);
    brect(bw-4, 0, 5, 1, 2);*/

    brect(0, 0, bw, 1, 3);
    brect(0, bh-1, bw, 1, 3);
    bmap[0] = 2;
    bmap[bw-1] = 2;
    bmap[bh*bw-bw] = 2;
    bmap[bh*bw-1] = 2;

    denx = div(mapsz, 2);
    deny = (div(bh, 3)-1)*2+4;
}

function printBase() {
    const ca = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    s = "";
    for(let y = 0; y < bh; y++) {
        for(let x = 0; x < bw; x++)
            s += ca[bmap[y*bw+x]] + " ";
        s += "\n";
    }
    console.log(s);
}

function inBounds(x, y) {
    return (x >= 0 && y >= 0 && x < mapsz && y < mapsz);
}

function inMapTile(x, y) {
    if(x < 0 || y < 0 || x >= mapsz || y >= mapsz) return 0;
    return map[y*mapsz+x];
}

function madj(x1, y1, t1, t2) {
    let n = 0;
    for(let i = 0; i < 4; i++) {
        let x = x1+dirs[i][0];
        let y = y1+dirs[i][1];
        let t = inMapTile(x, y);
        if(t >= t1 && t <= t2) n++;
    }
    return n;
}

function fillInaccessible(r) {
    const ph = -6;
    map[1*mapsz+1] += ph;
    let working;
    do {
        working = false;
        for(let i = 0; i < mapsz*mapsz; i++) {
            if(map[i] > ph) continue;
            for(let j = 0; j < 4; j++) {
                let x = i%mapsz+dirs[j][0];
                let y = div(i,mapsz)+dirs[j][1];
                if(!inBounds(x, y)) continue;
                let t = map[y*mapsz+x];
                if(t <= 0 && t > ph) {
                    map[y*mapsz+x] += ph;
                    working = true;
                }
            }
        }
    } while(working);
    map = map.map(t => (t <= 0 && t > ph) ? r : t);
    map = map.map(t => (t<=ph) ? t-ph : t);
}

function applyBase() {
    for(let i = 0; i < mapsz*mapsz; i++) map[i] = -1;
    for(let x = 0; x < bw; x++)
        for(let y = 0; y < bh; y++) {
            let t = bmap[y*bw+x];
            if(t == 0) continue;
            let c = t+2;
            if(c > 6) c = 1;
            map[y*2*mapsz+x*2] = c;
            if(bget(x, y+1) == t) map[(y*2+1)*mapsz+x*2] = c;
            if(bget(x+1, y) == t) map[y*2*mapsz+x*2+1] = c;
            //if(bget(x+1, y+1) == t) map[(y*2+1)*mapsz+x*2+1] = c;
        }

    for(let y = 0; y < mapsz; y++)
        for(let x = 0; x <= div(mapsz,2); x++)
            map[y*mapsz+div(mapsz,2)+x] = map[y*mapsz+div(mapsz,2)-x];

    /*for(let i = 0; i < mapsz*mapsz; i++)
        if(madj(i%mapsz, div(i,mapsz), 1, 16) == 4) map[i] = 1;*/

    fillInaccessible(tDen);
    //map[deny*mapsz+denx] = 3;
    map[(deny-4)*mapsz+denx] = tDen;
    map[(deny+4)*mapsz+denx] = tDen;
    fillInaccessible(1);

    /*for(let x = 0; x < mapsz; x++) {
        if(map[x] == -1) map[x] = map[(mapsz-1)*mapsz+x];
        else if(map[(mapsz-1)*mapsz+x] == -1) map[(mapsz-1)*mapsz+x] = map[x];
    }*/
    for(let x = 0; x < mapsz; x++) {
        if(map[x] == -1)
            map[x] = (map[(mapsz-1)*mapsz+x]!=-1) ? 5 : -1;
        else if(map[(mapsz-1)*mapsz+x] == -1)
            map[(mapsz-1)*mapsz+x] = (map[x]!=-1) ? 5 : -1;
    }

    fillInaccessible(5);
    for(let x = 0; x < mapsz; x++) {
        if(map[x] == -1)
            map[x] = (map[(mapsz-1)*mapsz+x]!=-1) ? 5 : -1;
        else if(map[(mapsz-1)*mapsz+x] == -1)
            map[(mapsz-1)*mapsz+x] = (map[x]!=-1) ? 5 : -1;
    }

    for(let i = 0; i < mapsz*mapsz; i++)
        if(map[i] > 0)
            if(madj(i%mapsz, div(i,mapsz), 5, 5) >= 1)
                map[i] = 5;

    let px = denx-1;
    let py = deny+5;

    corner = [
        [1, 1],
        [mapsz-2, 1],
    ];

    let y;
    for(y = div(mapsz, 2); map[y*mapsz+1] != -1; y++);
    corner[2] = [1, y];
    corner[3] = [mapsz-2, y];
    corner[4] = [denx-1, deny+5];

    for(let i = 0; i < 4; i++)
        map[corner[i][1]*mapsz+corner[i][0]] = tBluePellet;
}

function printMap() {
    let s = "";
    for(let y = 0; y < mapsz; y++) {
        for(let x = 0; x < mapsz; x++) {
            let t = map[y*mapsz+x];
            if(t <= 0) s += ". ";
            else s += map[y*mapsz+x] + " ";
        }
        s += "\n";
    }
    console.log(s);
}

function countPellets() {
    totalPellets = 0;
    npellets = 0;
    for(let i = 0; i < mapsz*mapsz; i++)
        if(map[i] == -1) totalPellets++;
}

function generateMap() {
    do {
        generateBase();
        //printBase();
        applyBase();
        //printMap();
        countPellets();
    } while(totalPellets < minPellets);
}
